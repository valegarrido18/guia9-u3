﻿#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

typedef struct Nodo{
    int Numero;
    struct Nodo *Lazo=NULL;
}Nodo;


class Hash{
    private:
        int Tamano_Arreglo;
        int *Arreglo;
    // Constructor por defecto
    public:
        Hash(int Tamano_Arreglo){
            this->Tamano_Arreglo = Tamano_Arreglo;
            this->Arreglo = new int[Tamano_Arreglo];
        }
        
    // Función para dejar cada posición del arreglo con vacío.
    void Arreglo_Vacio(){
        for(int i=0; i<Tamano_Arreglo; i++){
            Arreglo[i]='\0';
        }
    }
  
    // En cada if se ingresa el número ingesado en una posición en específico según su hash.
    void Llenar_Arreglo(string Hash_Seleccionado){
        int Numero_Ingresado;
        int Direccion;
        
        cout << "Ingrese un número: ";
        cin >> Numero_Ingresado;
        
        Direccion=Calcular_Hash_Modulo(Numero_Ingresado);
        
        if(Hash_Seleccionado == "L" || Hash_Seleccionado == "l"){
            if(Arreglo[Direccion] == '\0'){
                Arreglo[Direccion] = Numero_Ingresado;
                cout << "\n";
                Imprimir_Arreglo();
                cout << "\n";
            }
            else{
                cout << "Colisión en la posición: " << Direccion << endl;
                Ingreso_Lineal(Numero_Ingresado);
                Imprimir_Arreglo();
                cout << "\n";
            }
        }
        if(Hash_Seleccionado == "C" || Hash_Seleccionado == "c"){
            if(Arreglo[Direccion] == '\0'){
                Arreglo[Direccion] = Numero_Ingresado;
                cout << "\n";
                Imprimir_Arreglo();
                cout << "\n";
            }
            else{
                cout << "Colisión en la posición: " << Direccion << endl;
                Ingreso_Cuadratica(Numero_Ingresado);
                Imprimir_Arreglo();
                cout << "\n";
            }
        }
        if(Hash_Seleccionado == "D" || Hash_Seleccionado == "d"){
            if(Arreglo[Direccion] == '\0'){
                Arreglo[Direccion] = Numero_Ingresado;
                cout << "\n";
                Imprimir_Arreglo();
                cout << "\n";
            }
            else{
                cout << "Colisión en la posición: " << Direccion << endl;
                Ingreso_Doble_Direccion(Numero_Ingresado);
                Imprimir_Arreglo();
                cout << "\n";
            }
        }
        if(Hash_Seleccionado == "E" || Hash_Seleccionado == "e"){
            if(Arreglo[Direccion] == '\0'){
                Arreglo[Direccion] = Numero_Ingresado;
                cout << "\n";
                Imprimir_Arreglo();
                cout << "\n";
            }
            else{
                cout << "Colisión en la posición: " << Direccion << endl;
                Ingreso_Encadenamiento(Numero_Ingresado);
                Imprimir_Arreglo();
                cout << "\n";
            }
        }
    }  
    
    // Función para imprimir el arreglo.
    void Imprimir_Arreglo(){
        for(int i=0; i<Tamano_Arreglo; i++){
            cout << Arreglo[i] << "  ";
        }
    }
    
    // Función que busca un número ingresado por el usuario en base al hash seleccionado (l/c/d/e).
    void Buscar_Numero(string Hash_Seleccionado){
        int Numero_Buscado;
        int Direccion;
        
        cout << "Ingrese el número a buscar: ";
        cin >> Numero_Buscado;
        
        cout << "\n";
        Direccion = Calcular_Hash_Modulo(Numero_Buscado);
        
        if(Hash_Seleccionado == "L"||Hash_Seleccionado == "l"){
            if(Arreglo[Direccion] == Numero_Buscado){
                cout << "El número buscado esta en la posición: " << Direccion+1;
            }
            else{
                Prueba_Lineal(Numero_Buscado);
            }
        }
        if(Hash_Seleccionado == "C" || Hash_Seleccionado == "c"){
            if(Arreglo[Direccion] == Numero_Buscado){
                cout << "El número buscado esta en la posición: " << Direccion+1;
            }
            else{
                Prueba_Cuadratica(Numero_Buscado);
            }
        }
        if(Hash_Seleccionado == "D"|| Hash_Seleccionado == "d"){
            if(Arreglo[Direccion] == Numero_Buscado){
                cout << "El número buscado esta en la posición: " << Direccion+1;
            }
            else{
                Prueba_Doble_Direccion(Numero_Buscado);
            }
        }
        if(Hash_Seleccionado == "E" || Hash_Seleccionado == "e"){
            if(Arreglo[Direccion]==Numero_Buscado){
                cout << "El número buscado esta en la posición: " << Direccion+1;
            }
            else{
                Ingreso_Encadenamiento(Numero_Buscado);
            }
        }
    }
    
    // Se llama a la función Hash y se calcula la dirección. Convierte una c labe dada una dirección (indice) dentro de un arreglo.
    int Calcular_Hash_Modulo(int Clave){
        int N;
        N=Primo_Cercano();
        int Direccion=(Clave%N)+1;
        
        return Direccion;
    }
    
    
    // Otro método para calcular el hash.	
    int Calcular_Hash_Alternativo(int D){
        int Direccion;
        Direccion=((D+1)%Tamano_Arreglo)+1;
        
        return Direccion;
    }
    
    // Función para saber que número es primo.
    bool Primo(int I){
        int Contador=0;
        bool Band;
        for(int i=1; i<I+1; i++){
            if((I%i) == 0){
                Contador++;
            }
        }
        if(Contador==2){
            return Band=true;
        }
        else{
            return Band=false;
        }
    }
    
    // Función para buscar el número primo mas cercano, esto se utiliza para luego calcular el hash.
    int Primo_Cercano(){
        int I=Tamano_Arreglo-1;
        int Cercano=0;
        
        while(I!=1){
            if(Primo(I) == true){
                Cercano=I;
                break;
            }
            else{
                I--;
            }
        }
        return Cercano;
    }
    
    
    // Método de ingreso de la prueba lineal, donde con respecto al valor de DX se va a posición en el arreglo el número ingresado.
    void Ingreso_Lineal(int Numero_Ingresado){
        int D;
        int DX;
        D=Calcular_Hash_Modulo(Numero_Ingresado);
        
        if((Arreglo[D]!='\0') && (Arreglo[D] == Numero_Ingresado)){
            cout << "La información se encuentra en la posición: " << D << endl << endl;
        }
        else{
            DX=D+1;
            while((DX<Tamano_Arreglo) && (Arreglo[DX]!='\0') && (Arreglo[DX]!=Numero_Ingresado) && (DX!=D)){
                DX++;
                
                if(DX==(Tamano_Arreglo)){
                    DX=0;
                }   
            }
            
            if(Arreglo[DX] == '\0'){
                Arreglo[DX]=Numero_Ingresado;
                cout << "El número se movio a la posición: " << DX+1 << endl;
            }
        }
    }
    
    
    //Método de búsqueda de la prueba lineal que indica en que posición (DX) se encuentra el número.
    void Prueba_Lineal(int Numero_Buscado){
        int D;
        int DX;
        D=Calcular_Hash_Modulo(Numero_Buscado);
        
        if(Arreglo[D] != '\0' && Arreglo[D] == Numero_Buscado){
            cout << "La inforamción se encuentra en la posición: " << D+1;
        }
        else{
            DX=D+1;
            
            while(DX <= Tamano_Arreglo && Arreglo[DX] != '\0' && Arreglo[DX] != Numero_Buscado && DX!=D){
                DX++;
                
                if(DX == Tamano_Arreglo){
                    DX=0;
                }
            }
            if(Arreglo[DX] == '\0' || DX == D){
                cout << "La información no se encuentra en el arreglo";
            }
            else{
                cout << "La información se encuentra en la posicion: " << DX+1;
            }
        }
    }
    
    
    // Método de ingreso de la prueba cuadrática, donde con respecto al valor de DX se va a posición en el arreglo el número ingresado.
    void Ingreso_Cuadratica(int Numero_Ingresado){
        int D;
        int DX;
        int I;
        D=Calcular_Hash_Modulo(Numero_Ingresado);
        
        if((Arreglo[D]!='\0')&&(Arreglo[D]==Numero_Ingresado)){
            cout <<"informacion esta en la posicion : " << D+1 << endl;
        }
        else{
            I=0;    
            DX=D+(I*I);
            while((Arreglo[DX] != '\0') && (Arreglo[DX] != Numero_Ingresado)){
                I=I+1;  
                DX=D+(I*I);
                if(DX >= (Tamano_Arreglo+1)){
                    I=-1;
                    DX=1;
                    D=0;
                }
            }
            if(Arreglo[DX] == '\0'){
                Arreglo[DX]=Numero_Ingresado;
                cout << "El número fue desplazado a la posición: " << DX+1 << endl;
            }
            else{
                cout << "La información se encuentra en la posición: " << DX+1 << endl;
            }
        }
    }
    
    // Método de búsqueda de la prueba cuadratica que indica en que posición (DX) se encuentra el número.
    void Prueba_Cuadratica(int Numero_Buscado){
        int D;
        int DX;
        int I;
        D=Calcular_Hash_Modulo(Numero_Buscado);
        
        if((Arreglo[D] != '\0') && (Arreglo[D] == Numero_Buscado)){
            cout << "La información esta en la posición: " << D+1 << endl;
        }
        else{
            I=0;
            DX=D+(I*I);
            while((Arreglo[DX] != '\0') && (Arreglo[DX] != Numero_Buscado)){
                I++;
                DX=(D+(I*I));
                
                if(DX >= (Tamano_Arreglo+1)){
                    I=-1;
                    DX=1;
                    D=0;
                }
            }
        }
        if((Arreglo[DX] == '\0')){
            cout << "La información no se encuentra en el arreglo";
        }
        else{
            cout << "La información esta en la posición: " << DX+1 << endl;
        }
    }
    
    
    // Método de ingreso de la doble dirección, donde con respecto al valor de DX se va a posición en el arreglo el número ingresado.
    void Ingreso_Doble_Direccion(int Numero_Ingresado){
        int D;
        int DX;
        D=Calcular_Hash_Modulo(Numero_Ingresado);
        
        if((Arreglo[D] != '\0') && (Arreglo[D] == Numero_Ingresado)){
            cout << "El número esta en la posición: " << D+1 << endl;
        }
        else{
            DX=Calcular_Hash_Alternativo(D);   
            while((DX <= Tamano_Arreglo) && (Arreglo[DX] != '\0') && (Arreglo[DX != Numero_Ingresado]) && (DX != D)){
                DX=Calcular_Hash_Alternativo(DX);
                if(DX >= Tamano_Arreglo){
                   DX=0; 
                }
            }
            if((Arreglo[DX] == '\0') || (Arreglo[DX] != Numero_Ingresado)){
                Arreglo[DX] = Numero_Ingresado;
                if(DX!=Tamano_Arreglo+1){
                    cout << "Número fue desplazado a la posición: " << DX+1 << endl;
                }
            }
            else{
                cout << "La información se encuentra en la posición: " << DX+1 << endl;
            }
        }
    }
    
    
    // Método de búsqueda de la doble dirección que indica en que posición (DX) se encuentra el número.
    void Prueba_Doble_Direccion(int Numero_Buscado){
        int D;
        int DX;
        D=Calcular_Hash_Modulo(Numero_Buscado);
        
        if((Arreglo[D] != '\0') && (Arreglo[D] == Numero_Buscado)){
            cout << "El número buscado esta en la posición: " << D+1 << endl;
        }
        else{
            DX=Calcular_Hash_Alternativo(D);
            while((DX < Tamano_Arreglo) && (Arreglo[DX] != '\0') && (Arreglo[DX] != Numero_Buscado) && (DX != D)){
                DX=Calcular_Hash_Alternativo(DX);
                if(DX >= Tamano_Arreglo){
                    DX=0;
                }
            }
            if((Arreglo[DX] == '\0') || (Arreglo[DX] != Numero_Buscado)){
                cout << "El número buscado no esta en el arreglo";
            }
            else{
                cout << "El número buscado esta en la posicón: " << DX+1;
            }
        }
    }
    
    void Ingreso_Encadenamiento(int Numero_Ingresado){
        int D;
        Nodo *Q=NULL;
        Nodo *V[Tamano_Arreglo];
        Arreglo_Nuevo(V);
        D=Calcular_Hash_Modulo(Numero_Ingresado);
        
        if((Arreglo[D] != '\0') && (Arreglo[D] == Numero_Ingresado)){
            cout << "La información esta en la posición: " << D+1 << endl;
        }
        else{
            Q=V[D]->Lazo;
            while((Q != NULL) && (Q->Numero == Numero_Ingresado)){
                Q=Q->Lazo;
            }
            if(Q == NULL){
                cout << "La información no se encuentra en la lista" << endl;
            }
            else{
                cout << "La información se encuentra en la lista" << endl;
            }
        }
    }
    
    // Función para copiar el arreglo original a la lista de nodos.
    void Arreglo_Nuevo(Nodo *Arreglo_V[]){
        for(int i=0; i<Tamano_Arreglo; i++){
            Arreglo_V[i]->Numero='\0';
            Arreglo_V[i]->Numero=Arreglo[i];
            Arreglo_V[i]->Lazo=NULL;
        }
    }
};  


int main(int argc, char *argv[]) {
	if(argc != 2){
		cout << "Comando mal ejecutado, el programa no se podra iniciar" << endl << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada, de la siguiente forma: " << endl << endl;
		cout << "Ejemplo: ./Guia9 Letra(l/c/d/e)";
		cout << "\n";
		exit(-1);
	}
	else{
		string Hash_Seleccionado;
		Hash_Seleccionado=*argv[1];
		if(Hash_Seleccionado != "l"&&Hash_Seleccionado!="c" && Hash_Seleccionado != "d" && Hash_Seleccionado != "e"
		&& Hash_Seleccionado != "L"&&Hash_Seleccionado!="C" && Hash_Seleccionado != "D" && Hash_Seleccionado != "E"){
			cout << "El programa no se podra iniciar. Ingreso una letra de Hash incorrecto" << endl;
			cout << "Las opciones para hash son: l/c/d/e";
			cout << "\n";
			exit(-1);
		}
		else{
			string Entrada;
			int Respuesta_Menu;
			// Se define un arreglo de tamaño 15.
			Hash *Metodo_Hash = new Hash(15);
			
			cout << "         - MENU -             " << endl;
			cout << "---------------------------" << endl; 
			cout << ">> Ingresar Números     [1]" << endl;
			cout << ">> Buscar Números       [2]" << endl;
			cout << ">> Salir del programa   [0]" << endl << endl;
			cout << " " << endl;
			cout << "Ingrese opción: ";
			getline(cin,Entrada);
			
			while(Entrada != "0" && Entrada != "1" && Entrada != "2"){
				cout << "\n";
				cout << "Opción incorrecta. Eliga una opcion del menú: ";
				getline(cin,Entrada);
			}
			Respuesta_Menu=stoi(Entrada);
			
			do{
				switch(Respuesta_Menu){
					case 1: cout << "\n";
							Metodo_Hash->Llenar_Arreglo(Hash_Seleccionado);
							cout << "\n";
							cout << "         - MENU -             " << endl;
							cout << "---------------------------" << endl; 
							cout << ">> Ingresar Números     [1]" << endl;
							cout << ">> Buscar Números       [2]" << endl;
							cout << ">> Salir del programa   [0]" << endl << endl;
							cout << " " << endl;
							cout << "Ingrese opción: ";
							cin >> Entrada;
							
							while(Entrada != "0" && Entrada != "1" && Entrada != "2"){
								cout << "\n";
								cout << "Opción incorrecta. Eliga una opción del menú: ";
								cin >> Entrada;
							}
							Respuesta_Menu=stoi(Entrada);
							break;
					case 2: cout << "\n";
							Metodo_Hash->Buscar_Numero(Hash_Seleccionado);
							cout << "\n" << endl;
							cout << "         - MENU -             " << endl; 
							cout << "---------------------------" << endl; 
							cout << ">> Ingresar Números     [1]" << endl;
							cout << ">> Buscar Números       [2]" << endl;
							cout << ">> Salir del programa   [0]" << endl << endl;
							cout << " " << endl;
							cout << "Ingrese opción: ";
							cin >> Entrada;
							
							while(Entrada != "0" && Entrada != "1" && Entrada != "2"){
								cout << "\n";
								cout << "Opción incorrecta. Eliga una opción del menú: ";
								cin >> Entrada;
							}
							Respuesta_Menu=stoi(Entrada);
							break;
				}
			}while(Respuesta_Menu != 0);
			if(Respuesta_Menu == 0){
				cout << "\n";
				exit(-1);
			}
		}
	}
    return 0;
}

